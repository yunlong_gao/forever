
public abstract class ACar implements ICar{
	
	private String make;
	
	private String model;
	
	private int year;
	
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getMake() +""
	 
	
}
