/**
 *Different types in Java:
 * CLASS 
 * ENUM	
 * primitives
 * interfaces
 * 
 * a class is combination of data and methods
 * 
 * an interface only defines methods that is subclasses will hava
 * By definition, everything in an interface is public



  *creat an interface (let us program in genreal)
  *creat an abstract class (let us code shared functionally)
  *create concreate class
  *
  *Abastact:
  *-cannot be instantiiated
  *-concrete classes that inherit form the abstract class "" implement
  *alll the abstract methods 
  *-abstraqct subclass my or may not implent the abstract methods
  *
  *
  *implements means I'm inheriting from an interface
  *you can implement as many interface as you want
  *
  *
  *!!!!!!!!everything extands objects
 * @author unouser
 *
 */
public interface ICar 
{
	/** The make car */
	String getMake();
	
	/** the model*/
	String getModel();
	
	
	int getYear();
	
	int getMileage();
}
